import requests
import config
import json


def refresh_access_token():
    with open('refreshtoken.txt', 'r') as rt:
        refresh_token = rt.readline().rstrip()
        print refresh_token
    new_token = requests.post(config.api_url,
                              params={'grant_type': 'refresh_token',
                                      'refresh_token': refresh_token,
                                      'client_id': config.client_id,
                                      'client_secret': config.client_secret,
                                      "redirect_uri": "http://127.0.0.1/"},
                              )
    print(new_token.content)
    json_token_data = new_token.json()
    with open('accesstoken.txt', 'r+') as at:
        at.write(json_token_data['access_token'])
    with open('refreshtoken.txt', 'r+') as rf:
        rf.write(json_token_data['refresh_token'])
    return new_token

refresh_access_token()
