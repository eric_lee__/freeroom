# FreeRoom
A python app that connects with the [Eventboard api](https://eventboard.io/api/docs/) to check conference room availability and post the results to slack. 

Eventboard access tokens are good for ten hours.  Refresh.py can be run on a cron job to get new tokens on a periodic basis. 


###Config file

```
# config.py

api_url = 'Url of the eventboard api'
client_id = 'Your eventboard app client id'
client_secret = 'Your eventboard app client secret'
slack_webhook_token = 'Your slack webhook token'
rooms = {'Your room name #1': 'eventboard room id #1', 'Your room name #2': 'eventboard room id #2'...}
```