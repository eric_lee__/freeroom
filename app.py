import requests
import config
from flask import Flask, request
import time
import json
from threading import Thread
import csv


app = Flask(__name__)


@app.route('/ping')
def pingpong():
    return 'pong', 200


@app.route('/slash', methods=['POST'])
def listen_for_slack_webhooks():
    if request.form.get('token') == config.slack_webhook_token:
        print('valid token')
        url = request.form.get('response_url')
        slack_user = request.form.get('user_name')
        # threaded so that return to slack happens in less than three seconds
        slack_response_thread = Thread(target=return_open_rooms_to_slack, args=(url,slack_user))
        slack_response_thread.start()
        return '_Checking to see what rooms are free._', 200
    else:
        return '', 401


@app.route('/button', methods=['POST'])
def listen_for_slack_actions():
    post_data = request.form.get('payload')
    json_post_data = json.loads(post_data)
    if json_post_data['token'] == config.slack_webhook_token:
        print('valid token')
        url = json_post_data['response_url']
        room_name = json_post_data['actions'][0]['value']
        button_press_time = float(json_post_data['action_ts'])
        message_time = float(json_post_data['message_ts'])
        slack_user = json_post_data['user']['name']
        if button_press_time - message_time < 60:
            button_response_thread = Thread(target=book_eventboard_room, args=(room_name, slack_user, url))
            button_response_thread.start()
            return ':loading:', 200
        else:
            requests.post(url, json={'attachments': [
                {
                    "fallback": "This button has expired",
                    "color": "warning",
                    "author": "\n",
                    "title": '\n:warning: This button has expired. Please run the /freeroom command again.',
                }
            ]})
            return '', 200
    else:
        return '', 401


def get_reservation_data():
    with open('accesstoken.txt', 'r') as at:
        access_token = at.readline().rstrip()
        print access_token
    headers = {"Authorization": "Bearer " + access_token}
    meeting_info = {}
    for room_name, room_id in config.rooms.iteritems():
        full_meeting_data = requests.get(
            'https://app.eventboard.io/api/v4/calendars/reservations/?room_id=' + room_id + '&per_page=10&page=1',
            headers=headers)
        meeting_info[room_name] = full_meeting_data.json()
    return meeting_info


def check_reservation_dates():
    res_data = get_reservation_data()
    meeting_dict = {}
    # kludgy way to check if meetings pulled from eventboard api have already finished
    counter = 0
    for k, v in res_data.items():
        while True:
            if v['reservations'][counter]['ends_at'] - time.time() < 0:
                counter = counter + 1
            else:
                meeting_dict[k] = v['reservations'][counter]['starts_at']
                counter = 0
                break
    print meeting_dict
    return meeting_dict


def evalute_meeting_time():
    meeting_dict = check_reservation_dates()
    unix_time = int(time.time())
    for key,val in meeting_dict.items():
        # remove information about meetings happening in less than five minutes from now
        if val - unix_time > 300:
            meeting_dict[key] = val - unix_time
        else:
            del meeting_dict[key]
    return meeting_dict


def return_open_rooms_to_slack(response_url, slack_user):
    with open("commandrun.csv", "a") as cr:
        wr = csv.writer(cr)
        wr.writerow([time.time(), slack_user])
    open_rooms = evalute_meeting_time()
    if not open_rooms:
        requests.post(response_url, json={'text': 'Sorry, there are no free rooms.'})
    else:
        for key,val in open_rooms.items():
            if val < 1860:
                new_val = int(val/60)
                requests.post(response_url, json={'attachments':[
                    {
                        "fallback": "Free time for room",
                        "color": "#3AA3E3",
                        "text": "The {} is free for the next {} minutes".format(key, new_val),
                        "footer": "Rooms open for less than 30 minutes can't be booked from this app"
                    }
                ]})
            elif val < 3600:
                new_val = int(val/60)
                requests.post(response_url, json={'attachments':[
                    {
                        "fallback": "Free time for room",
                        "color": "#3AA3E3",
                        "callback_id": "bookroom",
                        "text": "The {} is free for the next {} minutes".format(key, new_val),
                        "actions": [
                            {
                                "name": "book",
                                "text": "Book Now",
                                "type": "button",
                                "style": "default",
                                "value": "{}".format(key)
                            }
                                    ]
                    }
                ]})
            else:
                new_val = int(val/3600)
                requests.post(response_url, json={'attachments':[
                    {
                        "fallback": "Freetime for room",
                        "color": "#3AA3E3",
                        "callback_id": "bookroom",
                        "text": "The {} is free for the next {} hour(s)".format(key, new_val),
                        "actions": [
                        {
                            "name": "book",
                            "text": "Book Now",
                            "type": "button",
                            "style": "default",
                            "value": "{}".format(key)
                        }
                                    ]
                    }
                ]})


def book_eventboard_room(room_name, slack_user, url):
    open_access_token = open('accesstoken.txt', 'r')
    access_token = open_access_token.readline().rstrip()
    open_access_token.close()
    headers = {"Authorization": "Bearer " + access_token, "Content-Type": "application/json"}
    bookdata = {
        "reservation": {
            "title": "{}'s Reserved Meeting".format(slack_user.capitalize()),
            "starts_at": time.time(),
            "ends_at":  time.time() + 1800
        }

    }
    requests.post(
        'https://app.eventboard.io/api/v4/calendars/reservations/?room_id=' + str(config.rooms[room_name]), data=json.dumps(bookdata),
        headers=headers)
    requests.post(url, json={'attachments': [
        {
            "fallback": "Room booked",
            "color": "good",
            "title": ':white_check_mark: You\'ve booked the {} for 30 minutes.'.format(room_name),
            "footer": "Thanks!"

        }
    ]})
    log_list = [time.time(), slack_user, room_name]
    with open("bookedrooms.csv", "a") as br:
        wr = csv.writer(br)
        wr.writerow(log_list)


if __name__=="__main__":
    app.run(host='0.0.0.0')
